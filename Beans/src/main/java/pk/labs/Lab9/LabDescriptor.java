package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = BeanTerm.class;
    public static Class<? extends Consultation> consultationBean = BeanConsultation.class;
    public static Class<? extends ConsultationList> consultationListBean = BeanConsultationList.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = BeanConsultationListFactory.class;
    
}
