/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author Marcin
 */
public class BeanConsultation implements Consultation, Serializable {

    private final PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vetoableChange = new VetoableChangeSupport(this);
    private String student;
    private Term term;

    public BeanConsultation() {
    }

    public Term getTerm() {
        return this.term;
    }

    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        String prvStudent = this.student;
        this.student = student;
        propertyChange.firePropertyChange("student", prvStudent, this.student);
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term prvTerm = this.term;
        vetoableChange.fireVetoableChange("term", prvTerm, term);
        this.term = term;
        propertyChange.firePropertyChange("term", prvTerm, term);
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if (minutes <= 0) {
            minutes = 0;
        }

        Term termNew = new BeanTerm();
        termNew.setBegin(this.term.getBegin());
        termNew.setDuration(this.term.getDuration() + minutes);
        int oldDuration = this.term.getDuration();
        this.vetoableChange.fireVetoableChange("term", term, termNew);
        this.term.setDuration(this.term.getDuration() + minutes);
        this.propertyChange.firePropertyChange("term", oldDuration, oldDuration + minutes);
    }
    
    public void addVeto ( BeanListener listener) {
        this.vetoableChange.addVetoableChangeListener(listener);
    }


}
