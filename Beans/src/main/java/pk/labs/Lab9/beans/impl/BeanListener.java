/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

/**
 *
 * @author Marcin
 */
public class BeanListener implements VetoableChangeListener{
    private BeanConsultationList List;

    public BeanListener(BeanConsultationList list) {
        this.List = list;
    }
    
    @Override
    public void vetoableChange(PropertyChangeEvent pce) throws PropertyVetoException {
        if (pce.getPropertyName().equals("term")) {
            BeanTerm oldTerm = (BeanTerm) pce.getOldValue();
            BeanTerm newTerm = (BeanTerm) pce.getNewValue();
            
            for (int i = 0; i < this.List.getSize()-1; i++) {
                if (this.List.getConsultation(i) == oldTerm) {
                    continue;
                }
                if (!List.getConsultation(i).getBeginDate().after(newTerm.getEnd())&&!List.getConsultation(i).getEndDate().before(newTerm.getBegin())) {
                    throw new PropertyVetoException("Listener PVE", null);
                }
            }
        }
    }
    
    
}
