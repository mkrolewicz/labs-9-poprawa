/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.awt.datatransfer.DataFlavor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.Serializable;
import java.util.Arrays;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

/**
 *
 * @author Marcin
 */
public class BeanConsultationList implements ConsultationList, Serializable {

    private PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    private BeanListener beanListener = new BeanListener(this);
    private Consultation[] list;
    
    public BeanConsultationList ()
    {
        list = new Consultation[0];
    }  
    
    @Override
    public int getSize() {
        return list.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return this.list;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.list[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        
        for (int i = 0; i < list.length; i++) 
        {
            if ((consultation.getEndDate().after(this.list[i].getBeginDate()) && consultation.getBeginDate().before(this.list[i].getBeginDate()))
                    || (consultation.getBeginDate().before(this.list[i].getEndDate()) && consultation.getEndDate().after(this.list[i].getBeginDate()))) 
            {
                throw new PropertyVetoException("consultation",null);
            }
        }
        
        Consultation[] newList = Arrays.copyOf(this.list, this.list.length+1);
        Consultation[] oldList = this.list;
        
        newList[newList.length-1] = consultation;
        ((BeanConsultation) consultation).addVeto(beanListener);
        this.list = newList;
        propertyChange.firePropertyChange("consultation", oldList, newList);
            
//        Consultation[] prvList = this.list;
//        int rozmiar = java.lang.reflect.Array.getLength(this.list);
//        Class typ = list.getClass().getComponentType();
//        int rozmiarM = Math.min(rozmiar, rozmiar+1);
//        Object nowe = java.lang.reflect.Array.newInstance(typ, rozmiar + 1);
//        this.list = (Consultation[])nowe;
//        this.list[this.list.length -1] = consultation;
        
        
//        propertyChange.firePropertyChange("consultation", prvList, this.list);
        
//        if (rozmiarM>0) 
//        {
//            System.arraycopy(this.list, 0, nowe, 0, rozmiarM);
//        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChange.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChange.addPropertyChangeListener(listener);
    }
    
}