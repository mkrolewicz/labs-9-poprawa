/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author Marcin
 */
public class BeanTerm implements Term, Serializable {

    //private PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    private int duration;
    private Date begin;
   
    
    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        //Date prvBegin = this.begin;
        this.begin = begin;
        //propertyChange.firePropertyChange("begin", prvBegin, this.begin);
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration>0) {
            //int prvDuration = this.duration;
            this.duration = duration;
            //propertyChange.firePropertyChange("duration", prvDuration, this.duration);
        }
    }

    @Override
    public Date getEnd() {
        return new Date(this.begin.getTime() + this.duration*60*1000);
    }
    
}
