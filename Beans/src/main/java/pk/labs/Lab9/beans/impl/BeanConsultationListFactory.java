/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import pk.labs.Lab9.beans.impl.*;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author Marcin
 */
public class BeanConsultationListFactory implements ConsultationListFactory, Serializable {

    private PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    
    @Override
    public ConsultationList create() {
        return new BeanConsultationList();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        BeanConsultationList consultationList = new BeanConsultationList();
        if (deserialize) {
            try {
                XMLDecoder xmlDecoder = new XMLDecoder(
                        new BufferedInputStream(new FileInputStream("serialization.xml")));
                consultationList.addConsultation((Consultation) xmlDecoder.readObject());
                xmlDecoder.close();
            } catch (FileNotFoundException | PropertyVetoException exception) {
                Logger.getLogger(BeanConsultationListFactory.class.getName()).log(Level.SEVERE,null,exception);
            }
            return consultationList;
        } else {
            return create();
        }
    }

    @Override
    public void save(ConsultationList consultationList) {
        try {
            try (XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                            new FileOutputStream("serialization.xml")))) {
                        encoder.writeObject(consultationList.getConsultation()[0]);
                        encoder.close();
                    }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BeanConsultationListFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
